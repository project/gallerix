<?php



class views_handler_field_album extends views_handler_field {

  /**
   * Constructor to provide additional field to add.
   */
  function construct() {
    parent::construct();
    //$this->additional_fields['max_users'] = 'max_users';
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $sizes = gallerix_sizes();
    $options = array();
    
    foreach ($sizes as $key => $size) {
      $options[$key] = $size['label'];
    }

    $form['thumbnail_size'] = array(
      '#type' => 'select',
      '#title' => t('Size'),
      '#options' => $options,
      '#default_value' => isset($this->options['thumbnail_size']) ? $this->options['thumbnail_size'] : 'thumbnail',
    );
  }

  function render($values) {
    $node = node_load($values->nid);
    
    gallerix_load_style();
    //return theme('gallerix_albums_grid', $node, $fielddata['options']);
    return theme('gallerix_albums_grid', $node, $this->options['thumbnail_size']);    
    
    //print_r($values);
    //return 'test';
    //return $difficulties[$values->{$this->field_alias}];


  }
}



