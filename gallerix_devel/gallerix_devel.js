
Gallerix.fn.updateDevel = function(image) {
  var content = '';
  content += '<pre style="line-height: 1em;">' + Gallerix.fn.printJson(image) + '</pre>';
  
  $('#gallerix-devel').html(content);
  Gallerix.flag('gallerix_devel');
}


Gallerix.fn.printJson = function(data) {
  
  var output = "";
  
  function _printJson(data, level) {
    
    if (level == undefined) {
      level = 0;
    }
    
    for (element in data) {
      
      output += '[' + element + ']';
      
      if (data[element] != null && typeof data[element] == 'object') {
        
        output += '<br />\n';
        output += '<div style="padding-left: 30px">';
          _printJson(data[element], level + 1);
        output += '</div>';
      }
      else {
        output += ' => ' + data[element] + '<br />\n';
      }
    } 
  }
  
  
  _printJson(data);
  
  return output;
}