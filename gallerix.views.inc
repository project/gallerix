<?php 

/**
 * @file
 * Provide views data and handlers for gallerix.module
 */



/**
 * Implementation of hook_views_data()
 */
function gallerix_views_data() {

  $data['gallerix_albums']['table']['group'] = t('Gallerix'); 
  $data['gallerix_albums']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      'type' => 'INNER',
    ),
  );
    
  $data['gallerix_albums']['path'] = array(
    'title' => t('Album Thumbnail'),
    'help' => t('The first picture in the album. A placeholder will be shown if the album is empty.'),
    'field' => array(
      'handler' => 'views_handler_field_album',
    ),
  
  );
  
  return $data;


}








/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * gallerix uses.
 */
function gallerix_views_handlers() {
  return array(
    'info' => array(
    ),
    'handlers' => array(
      // field handlers
      'views_handler_field_album' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}








/*


function gallerix_views_tables() {
  $tables['gallerix'] = array(
    'name' => 'gallerix',
    'fields' => array(
      'gallerix' => array(
        'name' => t('Gallerix: Album Thumbnail'),
        'handler' => array(
          'gallerix_views_handler_album' => t('Image'),
        ),
        'option' => array(
          '#type' => 'select',
          '#options' => 'gallerix_views_sizes',
        ),
        'notafield' => true,
        'sortable' => false,
      ),
    ),
  );
  return $tables;
}






function gallerix_views_sizes() {
  $sizes = gallerix_sizes();
  $content = array();
  
  foreach ($sizes as $key => $size) {
    $content[$key] = $size['label'];
  }
  
  return $content;
}


*/

/**
 * Views handler for displaying the image.
 */ /*
function gallerix_views_handler_album($fieldinfo, $fielddata, $value, $data) {
  $node = node_load($data->nid);
  
  gallerix_load_style();
  
  return theme('gallerix_albums_grid', $node, $fielddata['options']);
}

*/



/**
 * Theme the grid containing all albums.
 *
 */
function theme_views_view_gallerix_grid($view, $type, $nodes, $level = NULL, $args = NULL) {
  $output = '';
  $output .= '<div class="gallerix-grid-view">';
  
  gallerix_load_style();
  gallerix_load_js();

  foreach($nodes as $node) {
    $output .= theme('gallerix_albums_grid', $node);
  }
  
  $output .= '</div>';
  
  return $output;
}




/**
 * Themes each thumbnail to be displayed by the default view.
 *
 */
function theme_gallerix_albums_grid($node, $size = 'thumbnail') {

  gallerix_load_js();
  
  $node = node_load($node->nid);
  $translucent = variable_get('gallerix_translucence', 1) ? ' translucent' : '';
  
  if ($node->gallerix['default']) {
    $default = gallerix_load_picture($node->gallerix['default']);
    $thumbnail = theme('image', $default->$size, '', $node->title, array('class' => 'gallerix-external-thumbnail' . $translucent));
  
  }

  $link =  l($thumbnail, 'node/' . $node->nid, array('attributes' => array('class' => 'gallerix-external-picture-link'), 'html' => TRUE));
  
  $output .= $link;

  return $output;
}

