
var GallerixBuild = GallerixBuild || {};

$(document).ready(function() {

  GallerixBuild.settings = Drupal.settings.gallerixBuild;
  
  GallerixBuild.begin();
});






/**
 * Begin the building process. Loop continuously until there are no more
 * pictures to build in the album.
 *
 */ 
GallerixBuild.begin = function() {

  GallerixBuild.settings.threshold = 10;
  
  GallerixBuild.build(0, 0);

} 
 
 
 
GallerixBuild.build = function(current, completed) {
  var url = GallerixBuild.settings.buildURL + current + '/' + completed;
 
  $.get(url, null, function(data) {
    var response = Gallerix.parseJson(data);
    
    $('#progress > div.percentage').html(response.percentage + '%');
    $('#progress div.filled').css('width', response.percentage + '%');
    
      
    if (response.status == 'continue') {
      GallerixBuild.build(response.current, response.completed);
    }
    else {
      $('#progress > div.status').html("Building Complete");       
      $('#gallerix-build-album-form').get(0).submit();
    }
    
  });  
} 



