
$(document).ready(function () {
  gallerix_thumbnail_effects();

});



function gallerix_thumbnail_effects() {
  $('.gallerix-external-picture-link').each(function() {
    if (!$(this).is('.activated')) {
      $(this).css('opacity', .6);
      
      
      
      var mouseover = function () {
        $(this).css('opacity', 1);
      }
      var mouseout = function () {
        $(this).css('opacity', .6);
      }       
      
      $(this).mouseover(mouseover);
      $(this).mouseout(mouseout);
      $(this).addClass('activated');
    }
    
    
  });  
  

}