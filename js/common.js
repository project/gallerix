
//Namespace
var Gallerix = Gallerix || {};


$(document).ready(function () {
 
  Gallerix.thumbnailEffects();
  
});



Gallerix.thumbnailEffects = function () {
  $('.gallerix-external-thumbnail.translucent').each(function () {
    if (!$(this).is('.activated')) {
      $(this).css('opacity', .6);
      
      
      
      var mouseover = function () {
        $(this).css('opacity', 1);
      }
      var mouseout = function () {
        $(this).css('opacity', .6);
      }       
      
      $(this).mouseover(mouseover);
      $(this).mouseout(mouseout);
      $(this).addClass('activated');
    }
    
    
  });  
  

}







/************************** Utilities *********************************/


/**
 * Sets a temporary or indefinite message. There are two types of 
 * messages: status messages, and error messages.
 *
 *
 */
Gallerix.setMessage = function (message, type, time) {
  //Defaults
  if (type == undefined) {
    type = 'status';
  }
  if (time == undefined) {
    time = 0;
  }
  
  
  $('#gallerix-message').attr('class', 'messages ' + type).html(message).show(); 
  
  
  
  //If a time is specified, hide the message after the time has passed.
  if (time) {
    setTimeout(function () {
      Gallerix.clearMessage();
    }, time);
  }
};

//Wrapper function.
Gallerix.setError = function (message, time) {
  Gallerix.setMessage(message, 'error', time);
}


Gallerix.setStatus = function (message, time) {
  Gallerix.setMessage(message, 'status', time);
}



Gallerix.clearMessage = function () {
  $('#gallerix-message').hide().html('');
}



/**
 * A wrapper for parsing JSON. Unlike the original function, this
 * wrapper does not break when Devel is printing SQL queries. 
 *
 *
 */
Gallerix.parseJson = function (data) {

  // Only parse content in between the delimiters.
  var begin = data.indexOf('##BEGIN_JSON##');
  var end = data.lastIndexOf('##END_JSON##');
  var content = data.substring(begin + 14, end);
    
  var json =  eval('('+ content +')');
  
  json.remainder = data.substring(end + 12);  
  
  return json;
  
  
}




